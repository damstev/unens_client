'use strict';

angular.module('unensClient')
        .controller('NavbarCtrl', function ($scope, $location, Auth, Session) {
          $scope.date = new Date();

          $scope.isActive = function (viewLocation) {
            return viewLocation === $location.path();
          };

          $scope.isVisible = function (route) {
            //console.log(Auth.getLevel() + ' : ' + route + ' : ' + Auth.getVisibility(Auth.getLevel(), route));
            return Auth.getVisibility(Auth.getLevel(), route);
          };
          
          
          $scope.getUsername = function(){
            var userData = Session.getUserData();
            return userData.first_name;
          };
          
        });
