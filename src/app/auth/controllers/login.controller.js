'use strict';

angular.module('unensClient.auth')
        .controller('LoginCtrl', function ($scope, Auth, $location, Session, User, $http) {

          var fnIntialize = function fnIntialize() {
            $scope.user = {};
            $scope.error_msg = '';
          };

          ///Funcion para guardar 
          $scope.save = function (userform) {
            $scope.error_msg = '';
            if (userform.$valid) {
              Auth.login($scope.user).then(function (data) {
                console.log(data.data.data);
                Session.setSessionData(data.data.data);
                
                ///Agrego a la cabezera 
                $http.defaults.headers.common['X-Authorization'] = Session.getSessionData().key;
                
                User.get({id: data.data.data.user_id}, function(data){
                  Session.setUserData(data.data);
                }, function(reason){
                  console.log(reason);
                });
                
                $location.path('/');
              }, function (reason) {
                console.log(reason);
                var values = reason.data.error.message;
                angular.forEach(values, function (value, key) {
                  $scope.error_msg += value;
                });
              });
            }
          };

          ///Funcion para borrar el formulario
          $scope.clear = function (userform) {
            fnIntialize();
            if (userform) {
              userform.$setPristine();
              userform.$setUntouched();
            }
          };

          ///Dice si un campo es valido o no
          $scope.validateField = function (field) {
            return field.$invalid && field.$touched;
          };

          fnIntialize();
        });
