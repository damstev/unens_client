'use strict';

angular.module('unensClient.auth')
        .controller('ForgotPasswordCtrl', function ($scope, Auth) {
          var fnIntialize = function fnIntialize() {
            $scope.user = {};
            $scope.error_msg = '';
            $scope.success_msg = '';
          };


          ///Funcion para guardar 
          $scope.save = function (userform) {
            $scope.error_msg = '';
            if (userform.$valid) {
              Auth.forgotPassword($scope.user.email).then(function (data) {
                console.log(data);
                $scope.success_msg = data.data.ok.message;
              }, function (reason) {
                console.log(reason);
                $scope.error_msg = reason.data.error.message;
              });
            }
          };


          ///Funcion para borrar el formulario
          $scope.clear = function (userform) {
            fnIntialize();
            if (userform) {
              userform.$setPristine();
              userform.$setUntouched();
            }
          };

          ///Dice si un campo es valido o no
          $scope.validateField = function (field) {
            return field.$invalid && field.$touched;
          };

          fnIntialize();
        });
