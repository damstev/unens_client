'use strict';

angular.module('unensClient.auth')
        .controller('ResetPasswordCtrl', function ($scope, Auth, $stateParams, $timeout, $location) {
          var fnIntialize = function fnIntialize() {
            $scope.user = {
              token: $stateParams.token
            };
            $scope.error_msg = '';
            $scope.success_msg = '';
          };


          ///Funcion para guardar 
          $scope.save = function (userform) {
            $scope.error_msg = '';
            if (userform.$valid) {
              Auth.resetPassword($scope.user).then(function (data) {
                console.log(data);
                $scope.success_msg = data.data.ok.message;
                
                $timeout(function(){
                  $location.path('/login');
                }, 3000);
                
              }, function (reason) {
                console.log(reason);
                $scope.error_msg = reason.data.error.message;
              });
            }
          };


          ///Funcion para borrar el formulario
          $scope.clear = function (userform) {
            fnIntialize();
            if (userform) {
              userform.$setPristine();
              userform.$setUntouched();
            }
          };

          ///Dice si un campo es valido o no
          $scope.validateField = function (field) {
            return field.$invalid && field.$touched;
          };

          fnIntialize();
        });
