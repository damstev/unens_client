'use strict';

angular.module('unensClient.auth')
        .controller('LogoutCtrl', function ($scope, Auth, $location, Session) {
          Auth.logout($scope.user).then(function (data) {
            Session.setSessionData({});
            Session.setUserData({});
            $location.path('/login');
          }, function (reason) {
            $location.path('/home');
          });
        });
