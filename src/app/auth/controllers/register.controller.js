'use strict';

angular.module('unensClient.auth')
        .controller('RegisterCtrl', function ($scope, Auth, $location, Location, User) {

          var fnIntialize = function fnIntialize() {
            $scope.user = {
              country_id: 57,
              region_id: 76,
              city_id: 76001,
              promoter_id: null,
            };

            $scope.promoter_document = null;
            $scope.promoter_name = null;

            $scope.error_msg = '';
            $scope.countries = [{id: 57, name: 'Colombia'}];
            $scope.regions = [];
            $scope.cities = [];

            $scope.getRegions($scope.user.country_id);
          };

          ///Funcion para actualizar las regiones segun el país
          $scope.getRegions = function (country) {
            Location.getRegions(country).then(function (data) {
              $scope.regions = data.data.data;
              ///Asigno la primera region al user
              $scope.user.region_id = $scope.regions[0].id;
              ///Consulto las ciudades
              $scope.getCities($scope.user.region_id);
            }, function (reason) {
              console.log(reason);
            });
          };

          ///Funcion para actualizar las ciudades segun la region
          $scope.getCities = function (region) {
            Location.getCities(region).then(function (data) {
              $scope.cities = data.data.data;
              ///Asigno la primera ciudad al user
              $scope.user.city_id = $scope.cities[0].id;
            }, function (reason) {
              console.log(reason);
            });
          }

          ///Funcion para guardar 
          $scope.save = function (userform) {
            $scope.error_msg = '';
            if (userform.$valid) {
              Auth.register($scope.user).then(function (data) {
                $location.path('/welcome');
              }, function (reason) {
                var values = reason.data.error.message;
                angular.forEach(values, function (value, key) {
                  $scope.error_msg += value;
                });
              });
            }
          };

          ///Funcion para borrar el formulario
          $scope.clear = function (userform) {
            fnIntialize();
            if (userform) {
              userform.$setPristine();
              userform.$setUntouched();
            }
          };

          ///Dice si un campo es valido o no
          $scope.validateField = function (field) {
//            console.log(field.$name);
            return field.$invalid && field.$touched;
          };


          ///Funcion para buscar el promotor segun su documento
          $scope.searchPromoter = function (promoter_field) {
            var promoter_document = promoter_field.$modelValue;
            if (!promoter_document)
              return;
            User.get({username: promoter_document}, function (data) {
              if (_.isEmpty(data.data)) {
                $scope.promoter_name = null;
                $scope.user.promoter_id = null;
                promoter_field.$invalid = true;
              } else {
                $scope.user.promoter_id = data.data[0].id;
                $scope.promoter_name = data.data[0].first_name + ' ' + data.data[0].last_name;
              }
            }, function (error) {
              $scope.promoter_name = null;
              $scope.user.promoter_id = null;
            });

          }

          ///Restrinjo a solo numeros el documento y el promotor
          $scope.$watch('user.username', function (data) {
            if (data)
              $scope.user.username = data.replace(/[^\d]/g, '');
          });
          $scope.$watch('promoter_document', function (data) {
            if (data)
              $scope.promoter_document = data.replace(/[^\d]/g, '');
          });

          fnIntialize();

        });
