'use strict';

angular.module('unensClient.auth', [])
        .config(function () {

        })
        .run(function ($rootScope, $location, Auth) {

          ///Verifico cuando la url cambie.
          $rootScope.$on('$stateChangeStart', function (event, next, current) {
            
            ///Obtendo el nivel del usuario logueado
            var level = Auth.getLevel();
            var nextRoute = next.url;            
            
            ///Verificamos si el usuario tiene permisos para la ruta a la que quiere ingresar
            if (!Auth.getVisibility(level, nextRoute) ) {
              ///Para el usuario 0 (no-logueado) se manda al login
              if(level === 0)
                $location.path('/login');
              else
                $location.path('/home');
            }
          });
        });