'use strict';

angular.module('unensClient.auth')
        .factory('Auth', function (Config, $http, Session) {
          return {
            register: function (user) {
              var url = Config.domainUrl + "api/v1/auth";
              return $http.post(url, user);
            },
            login: function (user) {
              var url = Config.domainUrl + "api/v1/auth/login";
              return $http.post(url, user);
            },
            logout: function () {
              var url = Config.domainUrl + "api/v1/auth/logout";
              return $http.post(url, {}, this.getAuthHeader());
            },
            getAuthHeader: function () {
              var sessionData = Session.getSessionData();
            
              return {headers: {
                  'X-Authorization': sessionData.key}
              };
            },
            getLevel: function () {
              var sessionData = Session.getSessionData();
              if (!sessionData || !sessionData.key)
                return 0;
              else
                return parseInt(sessionData.level);
            },
            getPermisionArray: function () {
              var permissions = {
                0: ['/welcome', '/login', '/register', '/forgotpassword', 'resetpassword/:token', '/'],
                1: ['/home', '/logout', '/edit-user', '/'],
                2: ['/home', '/logout', '/edit-user', '/admin-users', '/'],
              };
              return permissions;
            },
            getVisibility: function (level, route) {
              var arr = this.getPermisionArray();
              return _.contains(arr[level], route);
            },
            forgotPassword: function (email) {
              var url = Config.domainUrl + "api/v1/auth/forgot";
              return $http.post(url, {email: email});
            },
            resetPassword: function (user) {
              var url = Config.domainUrl + "api/v1/auth/reset";
              return $http.post(url, user);
            }

          };
        })
        .factory('Session', function (Config, $sessionStorage) {
          return{
            getSessionData: function () {
              var unensSessionData = $sessionStorage.unensSessionData;
              if (!unensSessionData || !unensSessionData.key)
                return {key: null};
              else
                return unensSessionData;
            },
            setSessionData: function(data){
              $sessionStorage.unensSessionData = data;
            },
            getUserData: function () {
              var unensUserData = $sessionStorage.unensUserData;
              if (_.isEmpty(unensUserData))
                return {};
              else
                return unensUserData;
            },
            setUserData: function(data){
              $sessionStorage.unensUserData = data;
            },
            isLoggedIn: function(){
                var sessionData = this.getSessionData();
                return sessionData.key !== null;
            }
          }
        });
              