'use strict';

angular.module('unensClient.order')
        .controller('CreateOrderCtrl', function ($scope, $timeout, ngCart, $rootScope, Session, $location, Product, Config) {
            $scope.finishedWizard = function () {
                $scope.currentStep = 'Resumen';
            };

            $rootScope.error_msg = '';
            $rootScope.imgDomain = Config.domainUrl + 'images/product/';

            ///Para pasar del resumen debe haber algo en el carrito
            $scope.canExitSummary = function () {
                if (fnEmptyCart()) {
                    $rootScope.error_msg = 'Debe escoger al menos un producto.';
                    $scope.currentStep = 'Productos';
                    return false;
                }
                ///Si no está logueado lo envio al login
                if (!Session.isLoggedIn())
                    $location.path('/login');

                $rootScope.error_msg = '';
                return true;
            };

            ///Para pasar del catalogo debe haber algo en el carrito
            $scope.canExitCatalog = function () {
                if (fnEmptyCart()) {
                    $rootScope.error_msg = 'Debe escoger al menos un producto.';
                    return false;
                }
                $rootScope.error_msg = '';
                return true;
            };

            ///Funcion para revisar si el carrito está vacio
            var fnEmptyCart = function () {
                var items = ngCart.getItems();
                console.log(items);
                if (_.isEmpty(items))
                    return true;
                else
                    return false;
            };

            $scope.currentStep = '';
//            $scope.products = [
//                {
//                    id: 1,
//                    name: 'producto 1',
//                    imageUrl: 'http://ngcart.snapjay.com/img/phones/motorola-xoom-with-wi-fi.0.jpg',
//                    snippet: 'hola descripcion producto 1'
//                },
//                {
//                    id: 2,
//                    name: 'producto 2',
//                    imageUrl: 'http://ngcart.snapjay.com/img/phones/motorola-xoom.0.jpg',
//                    snippet: 'hola descripcion producto 2'
//                },
//                {
//                    id: 3,
//                    name: 'producto 3',
//                    imageUrl: 'http://ngcart.snapjay.com/img/phones/motorola-atrix-4g.0.jpg',
//                    snippet: 'hola descripcion producto 3'
//                }
//            ];

            Product.query().$promise.then(function(data){
                console.log(data);
                $scope.products= data.data;
            });

            $timeout(function () {
                if (!fnEmptyCart() && Session.isLoggedIn())
                    $scope.currentStep = 'Datos Envío';
            }, 200);


        });