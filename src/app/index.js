'use strict';

angular.module('unensClient', [
  'ngAnimate', 
  'ngCookies', 
  'ngTouch', 
  'ngSanitize', 
  'ngResource', 
  'ui.router', 
  'unensClient.auth', 
  'ngStorage', 
  'unensClient.user', 
  'unensClient.location', 
  'ui.grid', 
  'ui.grid.resizeColumns', 
  'ui.grid.edit',
  'ui.grid.cellNav',
  'unensClient.order',
  'mgo-angular-wizard',
  'ngCart',
  'unensClient.product'
])
        .config(function ($stateProvider, $urlRouterProvider) {
          $stateProvider
                  .state('main', {
                    abstract: true,
                    url: '',
                    templateUrl: 'app/layouts/main.html'
                  })
                  .state('main.home', {
                    url: '/home',
                    templateUrl: 'app/main/main.html',
                    controller: 'MainCtrl',
                  })
                  .state('main.login', {
                    url: '/login',
                    templateUrl: 'app/auth/views/login.html',
                    controller: 'LoginCtrl',
                  })
                  .state('main.register', {
                    url: '/register',
                    templateUrl: 'app/auth/views/register.html',
                    controller: 'RegisterCtrl',
                  }).state('main.welcome', {
                    url: '/welcome',
                    templateUrl: 'app/auth/views/welcome.html',
                  }).state('main.logout', {
                    url: '/logout',
                    controller: 'LogoutCtrl',
                  }).state('main.forgotpassword', {
                    url: '/forgotpassword',
                    controller: 'ForgotPasswordCtrl',
                    templateUrl: 'app/auth/views/forgotpassword.html',
                  }).state('main.resetpassword', {
                    url: '/resetpassword/:token',
                    controller: 'ResetPasswordCtrl',
                    templateUrl: 'app/auth/views/resetpassword.html',
                  }).state('main.edit-user', {
                    url: '/edit-user',
                    controller: 'UserEditCtrl',
                    templateUrl: 'app/user/views/userEdit.html',
                  }).state('main.admin-users', {
                    url: '/admin-users',
                    controller: 'UserAdminCtrl',
                    templateUrl: 'app/user/views/userAdmin.html',
                  }).state('main.createorder', {
                    url: '/',
                    controller: 'CreateOrderCtrl',
                    templateUrl: 'app/order/views/createOrder.html',
                  });
          $urlRouterProvider.otherwise('/');
        });

        
