'use strict'

angular.module('unensClient.user')
        .factory('Product', function ($resource, Config) {
            var url = Config.domainUrl + "api/v1/product/:id";
            var Product = $resource(url, {id: '@id', },
                    {
                        query:{
                            isArray: false
                        },
                        get: {
                            method: 'GET',
                            params: {id: '@id'},
                        },
                        update: {
                            method: 'PUT',
                            params: {id: '@id'}
                        },
                    });

            return Product;
        });