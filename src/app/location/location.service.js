'use strict';

angular.module('unensClient.location')
        .factory('Location', function (Config, $http) {
          return {
            getCountries: function () {
              var url = Config.domainUrl + "api/v1/location/countries";
              return $http.get(url);
            },
            getRegions: function (country) {
              var url = Config.domainUrl + "api/v1/location/regions";
              return $http.get(url, {params: {country: country}});
            },
            getCities: function (region) {
              var url = Config.domainUrl + "api/v1/location/cities";
              return $http.get(url,{params: {region: region}});
            },
          };
        });