'use strict';

angular.module('unensClient.user')
        .controller('UserAdminCtrl', function ($scope, User, $http, Session, UserStatusesConstant) {
            $scope.error_msg = '';
            $scope.success_msg = '';

            $scope.gridOptions = {
                data: 'users',
                multiSelect: false,
                enableRowSelection: false,
                enableCellEditOnFocus: true
            };

            ///Extarnal scope para el ngGrid
            $scope.gridScope = $scope;

            $scope.gridOptions.columnDefs = [
                {field: 'username', displayName: 'Documento', enableCellEdit: false},
                {field: 'first_name', displayName: 'Nombre', enableCellEdit: false},
                {field: 'last_name', displayName: 'Apellido', enableCellEdit: false},
                {field: 'promoter_name', displayName: 'Promotor', enableCellEdit: false},
                {field: 'rol.name', displayName: 'Rol', enableCellEdit: false},
                {field: 'level.name', displayName: 'Nivel', enableCellEdit: false},
                {field: 'status_id', displayName: 'Estado', editDropdownOptionsArray: UserStatusesConstant, cellFilter: 'mapUserStatus', editableCellTemplate: 'ui-grid/dropdownEditor', cellTemplate: 'app/user/views/userStatusGridTmpl.html'},
                {field: 'created_at.date', displayName: 'Fecha Registro', type: 'date', enableCellEdit: false},
            ];

            $scope.gridOptions.onRegisterApi = function (gridApi) {
                //set gridApi on scope
                $scope.gridApi = gridApi;

                gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
                    $scope.error_msg = $scope.success_msg  = '';
                    if(newValue !== oldValue)
                        User.update({id: rowEntity.id}, rowEntity).$promise.then(function(data){
                            $scope.success_msg = 'Usuario modificado correctamente.';
                            $scope.refreshData();
                        }, function(error){
                            $scope.error_msg = 'Error al modificar el usuario';
                        });
                });
                
                gridApi.edit.on.beginCellEdit($scope,function(rowEntity, colDef){
//                    alert('hola');
                });                
            };

            $scope.users = [];

            $scope.refreshData = function () {
                $http.defaults.headers.common['X-Authorization'] = Session.getSessionData().key;
                User.get().$promise.then(function (data) {
                    console.log(data);
                    $scope.users = data.data;
                });
            };

            $scope.refreshData();

        });
//        .directive('ngBlur', function () {
//            return function (scope, elem, attrs) {
//                elem.bind('blur', function () {
//                    scope.$apply(attrs.ngBlur);
//                });
//            };
//        });