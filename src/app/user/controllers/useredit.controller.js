'use strict';

angular.module('unensClient.user')
        .controller('UserEditCtrl', function ($scope, User, Location, Session, $http) {

          $scope.error_msg = '';
          $scope.success_msg = '';
          $scope.countries = [{id: '57', name: 'Colombia'}];
          $scope.regions = [];
          $scope.cities = [];
          $scope.user = {};

          var fnInit = function () {
            ///Agrego a la cabezera 
            $http.defaults.headers.common['X-Authorization'] = Session.getSessionData().key;
            User.get({id: Session.getSessionData().user_id}).$promise.then(function (data) {
              $scope.user = data.data;
              $scope.getRegions($scope.user.country_id);
            });
          };



          ///Funcion para actualizar las regiones segun el país
          $scope.getRegions = function (country) {
            Location.getRegions(country).then(function (data) {
              $scope.regions = data.data.data;
              ///Asigno la primera region al user
              //$scope.user.region_id = $scope.regions[0].id;
              ///Consulto las ciudades
              $scope.getCities($scope.user.region_id);
            }, function (reason) {
              console.log(reason);
            });
          };

          ///Funcion para actualizar las ciudades segun la region
          $scope.getCities = function (region) {
            Location.getCities(region).then(function (data) {
              $scope.cities = data.data.data;
              ///Asigno la primera ciudad al user
              //$scope.user.city_id = $scope.cities[0].id;
            }, function (reason) {
              console.log(reason);
            });
          };

          ///Funcion para guardar 
          $scope.save = function (userform) {
            $scope.error_msg = '';
            $scope.success_msg = '';
            if (userform.$valid) {
              console.log('actualizand');
              User.update({id: $scope.user.id}, $scope.user).$promise.then(function (data) {
                $scope.success_msg = 'Datos Actualizados Correctamente';
                $scope.clear(userform);
              }, function (error) {
                console.log(error);
                $scope.error_msg = error.data.error.message;
              });
            }
          };

          ///Funcion para borrar el formulario
          $scope.clear = function (userform) {
            fnInit();
            if (userform) {
              userform.$setPristine();
              userform.$setUntouched();
            }
          };

          ///Dice si un campo es valido o no
          $scope.validateField = function (field) {
            return field.$invalid && field.$touched;
          };

          fnInit();

        });
