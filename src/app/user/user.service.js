'use strict'

angular.module('unensClient.user')
        .factory('User', function ($resource, Config, Session) {
            var url = Config.domainUrl + "api/v1/user/:id";
            var sessionData = Session.getSessionData();
            var User = $resource(url, {id: '@id', },
                    {
                        get: {
                            method: 'GET',
                            params: {id: '@id'},
                        },
                        update: {
                            method: 'PUT',
                            params: {id: '@id'}
                        },
                    });

            return User;
        })
        .factory('UserStatusesConstant', function () {
            return [
                {id: '1', value: "Registrado"},
                {id: '2', value: "Afiliado"},
                {id: '3', value: "Emprendedor"},
                {id: '4', value: "Inactivo"},
                {id: '5', value: "Eliminado"}
            ];
        });