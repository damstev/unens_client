'use strict'

angular.module('unensClient.user')
        .filter('mapUserStatus', function (UserStatusesConstant) {
            return function (input) {
                var result = _.findWhere(UserStatusesConstant, {id: input});
                
                if (!_.isEmpty(result)) {
                    return result.value;
                } else {
                    return 'unknown';
                }
            };
        })